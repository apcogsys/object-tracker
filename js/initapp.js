// Config Code is defined in settings.configcode by JSONEditor4Menu

// BEGIN: init code for menu item Load Config
var lf4d = new LoadFile4DOM();
var options = {
          "debug": false // if true, it will show the hidden <input type="file" ...> loaders in DOM
        };
lf4d.init(document,options);
//-----------------------------------------------
//----- Create a new Loader "txtfile" -----------
//-----------------------------------------------
// with MIME type filter use type="text"
//var txtfile = lf4d.get_loader_options("mytxtfile","text");

// if arbitray files are allowed use type="all"
var txtfile = lf4d.get_loader_options("jsontxtfile","text");
// Define what to do with the loaded data
txtfile.returntype = "file"; // data contains the file
console.log("txtfile: "+JSON.stringify(txtfile));
txtfile.onload = function (data,err) {
  if (err) {
    // do something on error, perr contains error message
    console.error(err);
    alert("ERROR: "+err)
  } else {
    // do something with the file content in data e.g. store  in a HTML textarea (e.g. <textarea id="mytextarea" ...>
    console.log("CALL: txtfile.onload()");
    document.getElementById("app_editor").value = data;
    app.nav.page('edit');
  }
};
// create the loader txtfile
lf4d.create_load_dialog(txtfile);
//-----------------------------------------------
//-----------------------------------------------
//----- Create a new Loader "jsonfile" -----------
//-----------------------------------------------
// with MIME type filter use type="text"
//var txtfile = lf4d.get_loader_options("mytxtfile","text");

// if arbitray files are allowed use type="all"
var jsonfile = lf4d.get_loader_options("jsonfile","json");
// Define what to do with the loaded data
jsonfile.returntype = "filehash"; // data contains the file
console.log("jsonfile: "+JSON.stringify(jsonfile));
jsonfile.onload = function (data,err) {
  if (err) {
    // do something on error, perr contains error message
    console.error(err);
    alert("ERROR: "+err)
  } else {
    // do something with the file content in data e.g. store  in a HTML textarea (e.g. <textarea id="mytextarea" ...>
    console.log("CALL: jsonfile.onload()");
    //parse_archive_arr(data.file);
    parse_loaded_json(data.file);
    //document.getElementById("app_editor").value = data.file;
    document.getElementById("app_editor").value = JSON.stringify(track_data,null,4);
    app.nav.page('edit');
  }
};
// create the loader txtfile
lf4d.create_load_dialog(jsonfile);
//-----------------------------------------------

// END: init code for menu item Load Config

function get_time_stamp() {
  var ts = "";
  var now = new Date();
  ts += now.getFullYear();
  var vMonth = now.getMonth();
  ts += ((vMonth < 10) ? "0" + vMonth : vMonth);
  var vDay = now.getDate();
  ts += ((vDay < 10) ? "0" + vDay : vDay);
  ts += "_";
  var vHour = now.getHours();
  ts += ((vHour < 10) ? "0" + vHour : vHour);
  var vMin = now.getMinutes();
  ts +=  ((vMin < 10) ? "0" + vMin : vMin);
  return ts;
}

function add_time_stamp2filename(pFilename) {
  var a = pFilename.split(".");
  if (a.length > 1) {
    a[a.length-2] += "_" + get_time_stamp();
    pFilename = a.join(".");
  };
  return pFilename;
}

// BEGIN: init code for menu item Save Config
function saveFile2HDD(pFilename,pContent) {
  var file = new File([pContent], {type: "text/plain;charset=utf-8"});
  saveAs(file,pFilename);
  app.nav.page('edit');
}

// END: init code for menu item Save Config


  //--- init pages ----
  app.nav.page("tracking");

  //--- Assign Toggle Menu Function ----
  $('.menu-toggle').click(app.nav.menu.toggle);

  // the configuration code can be used to create some constants, local functions or initialize some javascript objects.
