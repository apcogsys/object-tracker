**Colour Object Detection and Tracking**

Colour Object Detection and Tracking is an open source software product with an HTML/JS demonstrator that, as a web-based application in a browser. This application can recognize plates with a different colour (three default colours magenta, yellow and cyan) in a webcam, calculate their position in a coordinate system, and make them available as an output file in JSON format in a browser.  The application is able to run alone in the browser without an Internet connection.

The  WebApp is started as an HTML file locally with a browser locally on the computer. A WebCam is used to manipulate plates in a camera area and the WebApp records the positions of the plates. The JSON file is created dynamically in the browser and created with JSON.stringify(...) as string. This will then use FileSaver.js as a download to save the user locally on the computer. Due to the use of no Internet, connection is necessary for local storage on the computer. At time intervals, images are transfered from the camera to the tracker for object recognition.

**Installation**:

On the client side, no installation is necessary. You just point your browser to the URL of your deployment.

To run application with localhot server setup.
    
1. Install Node.js `https://nodejs.org/en/download/`

2. In cmd check npm(node package manage) is configured or not with `npm --version`

3. Download npm package called http-server. `npm install -g http-server`

4. In terminal go to project path and just run cmd: - `http-server`

5. In response you will get list of localhost urls, use any one to head towards application. (ex: http://127.0.0.1:8080/index.html)
    
	
**USEAGE**:

•	In order to start the process of object detection and tracking click the "StartTracking” button on the webapp and allow the camera access.

•	If you want to stop the tracking process at any point then click on the "Stoptracking" button.

•	To save the file locally click on "save JSON" button. This will save the file with the current date/time stamp as the filename and JSON as extension.

**Version**:

This is the first version being deployed "v1".
	

**Development Platform:**

The Webapp was developed and tested on windows 10 platform.
	
**Working Environment:**

The webapp works with Google chrome and Mozilla Firefox. Internet connection is not required to run this webapp i.e., it also works offline.
	
**Libraries**:

This make use of two main JavaScript libraries:
    
1]  TrackingJS (https://trackingjs.com) to use objects with the WebCam or the camera of a mobile device and let the recognition process be taken over by the already developed JavaScript library .

2]   FileSaver.js (https://github.com/eligrey/FileSaver.js) is a library to store data on the local computer (via the download functionality of browsers) in a WebApp. Therefore, no implementation of a server application is necessary.

**Execution file**:

The file that has to be executed to run the webapp is "/index.html"
 	
**Input**:

The input to the webapp is the video captured by the webcam.
	
**Output**:

The webapp output gives you the objects of different colours (magenta, yellow and cyan) that are marked with a circle at the centre of the object and has a number that indicates the object id of that specific object. There is also a button that create a JSON file that contains the details of the tracking objects and the file is saved locally.
